#!/bin/bash

## Usage: put this in the same folder as rigcheck.sh, it will copy that to each ethos machine in ethoservers

## Insert IP addressess for your Ethos Rigs. CHANGE THESE EXAMPLES
ethoservers=( "192.168.1.2" "192.168.1.3" "192.168.1.4")

for sname in "${ethoservers[@]}"
do
	#This one Copies the rigcheck script
	scp ./rigcheck.sh ethos@$sname:/home/ethos/

	#Uncomment this when you are intalling the first time/updating the config file
	#scp ./rigcheck.config ethos@$sname:/home/ethos/

	ssh ethos@$sname chmod a+x /home/ethos/rigcheck.sh
	#TODO add crontab entry if not already there
done
